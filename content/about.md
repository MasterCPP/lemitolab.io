+++
title = "Обо мне"
+++


## Contact

See the footer for my email and social media links.

## Credits

This website would not be possible without the generous work of others:

* [Zola](https://www.getzola.org/) static site compiler.
* [Manrope](https://manropefont.com/) font.